#!/usr/bin/env sh

set -e -u -x
set -o pipefail

export GOPATH=$PWD
export CONCOURSEDIR=$PWD

mkdir -p src/gitlab.com/demilletech/

cp -R repo src/gitlab.com/demilletech/fast-tracking-proxy

cd src/gitlab.com/demilletech/fast-tracking-proxy

GIT_ID=$(git log -1 --pretty=format:'%h')
GIT_MSG=$(git log -1 --pretty=format:'%B')
GIT_AUTHOR=$(git log -1 --pretty=format:'%an')
GIT_VERSION=$(cat .git/ref)

echo "fast-tracking-proxy $GIT_VERSION built successfully" >> $CONCOURSEDIR/message/passed
echo "fast-tracking-proxy $GIT_VERSION failed to build" >> $CONCOURSEDIR/message/failed
echo "fast-tracking-proxy $GIT_VERSION build was aborted" >> $CONCOURSEDIR/message/aborted

./scripts/build.sh

mv assets/* $CONCOURSEDIR/bin/