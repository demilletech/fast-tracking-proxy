package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/reuseport"
	"gitlab.com/demilletech/fast-tracking-proxy/conf"
	"gitlab.com/demilletech/fast-tracking-proxy/mq"
	"gitlab.com/demilletech/fast-tracking-proxy/proxy"
)

var sigs chan os.Signal
var s *fasthttp.Server
var servingOn string
var servingType string

func main() {
	//defer profile.Start().Stop()
	logrus.Debug("Starting program")
	conf.InitConfig()
	initVars()
	initProgram()

	go listenAndServe()

	runtime.Gosched()

	sig := <-sigs
	logrus.WithFields(logrus.Fields{"signal": sig.String()}).Info("Received signal, killing")
	mq.Close()
	s.Shutdown()

	logrus.Info("Goodbye")
}

func listenAndServe() {
	var ln net.Listener
	var err error
	switch servingType {
	case "unix":
		logrus.Debug("UNIX listen selected")
		ln, err = net.Listen("unix", servingOn)
		if err != nil {
			logrus.Fatal(err)
		}
		logrus.Debug("Setting permissions of UNIX serving socket")
		err = os.Chmod(servingOn, 0777)
		if err != nil {
			logrus.Fatal(err)
		}
	case "http":
		logrus.Debug("HTTP/TCP listen selected")
		ln, err = reuseport.Listen("tcp4", servingOn)
		if err != nil {
			logrus.Fatal(err)
		}
	default:
		logrus.Fatal("Unrecognized listen address")
	}

	logrus.Infof("Listening at %s, proxying to %s\n", servingOn, conf.ServerConfiguration.ProxyOrigin)
	err = s.Serve(ln)
	if err != nil {
		log.Fatal(err)
	}
}

func initProgram() {
	mq.Init()
}

func initVars() {
	logrus.Debug("Initializing vars")
	sigs = make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	if conf.ServerConfiguration.ListenUnix != "" {
		logrus.Debug("Initializing UNIX listener")
		servingOn = fmt.Sprintf("%s", conf.ServerConfiguration.ListenUnix)
		servingType = "unix"
	} else {
		logrus.Debug("Initializing TCP listener")
		servingOn = fmt.Sprintf("%s:%d", conf.ServerConfiguration.Host, conf.ServerConfiguration.Port)
		servingType = "http"
	}

	//h := fasthttp.CompressHandler(proxy.HandleRequest)
	h := proxy.HandleRequest
	s = newHTTPServer(h)
	logrus.Debug("Done initializing vars")
}

func newHTTPServer(h fasthttp.RequestHandler) *fasthttp.Server {
	return &fasthttp.Server{
		Handler:              h,
		ReadTimeout:          5 * time.Second,
		WriteTimeout:         10 * time.Second,
		MaxConnsPerIP:        500,
		MaxRequestsPerConn:   500,
		MaxKeepaliveDuration: 5 * time.Second,
	}
}
