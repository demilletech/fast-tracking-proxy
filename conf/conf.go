package conf

import (
	"io/ioutil"
	"strings"

	"github.com/BurntSushi/toml"
	"github.com/sirupsen/logrus"
)

type Config struct {
	Port                   int
	Host                   string
	ListenUnix             string   `toml:"listen_unix"`
	ProxyOrigin            string   `toml:"proxy_origin"`
	ProxyOriginUnix        string   `toml:"proxy_origin_unix"`
	RemoteIPHeader         string   `toml:"remote_ip_header"`
	RemoteIPShouldFallback bool     `toml:"remote_ip_should_fallback"`
	ServerName             string   `toml:"server_name"`
	MQService              string   `toml:"mq_service"`
	LogLevel               string   `toml:"log_level"`
	TrackedCookies         []string `toml:"tracked_cookies"`
	MQServiceConfig        map[string]string
	IsUnixOrigin           bool
	LogFormat              string `toml:"log_format"`
}

var ServerConfiguration Config

func InitConfig() {
	logrus.Info("Reading config file")
	tomlData, err := ioutil.ReadFile("config.toml")
	if err != nil {
		logrus.Fatal(err)
	}

	if _, err = toml.Decode(string(tomlData), &ServerConfiguration); err != nil {
		logrus.Fatal(err)
	}

	switch strings.ToLower(ServerConfiguration.LogLevel) {
	case "trace":
		logrus.SetLevel(logrus.TraceLevel)
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	case "fatal":
		logrus.SetLevel(logrus.FatalLevel)
	case "panic":
		logrus.SetLevel(logrus.PanicLevel)
	default:
		logrus.Warn("Unknown logging level, using defaults")
	}

	switch strings.ToLower(ServerConfiguration.LogFormat) {
	case "json":
		logrus.SetFormatter(&logrus.JSONFormatter{})
	case "text":
		logrus.SetFormatter(&logrus.TextFormatter{})
	}

	if ServerConfiguration.ProxyOriginUnix != "" {
		ServerConfiguration.ProxyOrigin = ServerConfiguration.ProxyOriginUnix
		ServerConfiguration.IsUnixOrigin = true
	}
	if ServerConfiguration.IsUnixOrigin {
		logrus.Debug("Origin is a unix socket")
	}

	logrus.Debug("Completed reading config file")
}
