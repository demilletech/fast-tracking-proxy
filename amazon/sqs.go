package amazon

import (
	"encoding/base64"

	"github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

var svc *sqs.SQS
var qURL string

func Init(mqServiceConfig map[string]string) {
	logrus.Infof("Initializing Amazon SQS")

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	svc = sqs.New(sess)

	var ok bool
	if qURL, ok = mqServiceConfig["queue_url"]; !ok {
		logrus.Panic("queue_url missing from config")
	}

}

func amazonPushData(data []byte) {
	logrus.Trace("Pushing data to SQS")

	encodedData := base64.StdEncoding.EncodeToString(data)

	result, err := svc.SendMessage(&sqs.SendMessageInput{
		MessageBody: aws.String(encodedData),
		QueueUrl:    &qURL,
	})

	if err != nil {
		logrus.Error(err)
		return
	}

	logrus.Infof("Published a message; msg ID: %v\n", *result.MessageId)
}

func PushData(data []byte) {
	amazonPushData(data)
}
