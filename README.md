# fast-tracking-proxy

This is a small Go server that proxies HTTP requests to "somewhere else," while logging a series of metrics about each transaction. The metadata is then passed along a Messaging Queue service, in zstd compressed JSON. Every request is assigned a unique `request-id`/`Request-ID` header, passed to the origin server and client respectively.

## Table of Contents

<!-- toc -->

- [Installation](#installation)
  * [Downloading a tarball](#downloading-a-tarball)
  * [Compiling from source](#compiling-from-source)
  * [Requirements](#requirements)
- [Structure](#structure)
- [Message Queue Services](#message-queue-services)
- [Configuration](#configuration)
  * [Proxy server](#proxy-server)
      - [port - _Required if listen_unix not provided_](#port---_required-if-listen_unix-not-provided_)
      - [host - _Required if listen_unix not provided_](#host----_required-if-listen_unix-not-provided_)
      - [listen_unix - _Required if host not provided_](#listen_unix----_required-if-host-not-provided_)
      - [proxy_origin - _Required if proxy_origin_unix not provided_](#proxy_origin---_required--if-proxy_origin_unix-not-provided_)
      - [proxy_origin_unix - _Required if proxy_origin not provided_](#proxy_origin_unix---_required-if-proxy_origin-not-provided_)
      - [remote_ip_header - _Optional_](#remote_ip_header----_optional_)
      - [remote_ip_should_fallback - _Optional_](#remote_ip_should_fallback---_optional_)
      - [server_name - _Required_](#server_name---_required_)
      - [mq_service - _Required_](#mq_service---_required_)
      - [log_level - _Optional_](#log_level---_optional_)
      - [log_format - _Optional_](#log_format---_optional_)
      - [tracked_cookies - _Optional_](#tracked_cookies---_optional_)
  * [MQ Service](#mq-service)
    + [Amazon SQS](#amazon-sqs)
      - [queue_url](#queue_url)
    + [Google PubSub](#google-pubsub)
      - [pubsub_topic_name](#pubsub_topic_name)
      - [pubsub_project_name](#pubsub_project_name)
- [Contributing a new MQ service](#contributing-a-new-mq-service)
  * [Code](#code)
  * [Config](#config)
  * [Selection](#selection)
- [Bugs](#bugs)
  * [Security vulnerabilities](#security-vulnerabilities)
- [Contributing](#contributing)
- [API Stability](#api-stability)
- [Limitations](#limitations)

<!-- tocstop -->

## Installation

To use this software, there are two main techniques.

### Downloading a tarball

There are prepared tarballs in some flavors of operating systems available on the [GitLab Releases page](https://gitlab.com/demilletech/fast-tracking-proxy/releases). Download the latest tarball, extract it, and place it in a usable location. Then, ensure it has a [configuration file](#configuration) available (see later in this document, or `config_default.toml` in the repository) as `config.toml`, and run.

### Compiling from source

These commands will prepare a binary called `proxy`/`proxy.exe`, depending on if you are on a Unix system or not, along with copying `config_default.toml` to create the new file `config.toml`.

```shell
git clone https://gitlab.com/demilletech/fast-tracking-proxy.git
cd fast-tracking-proxy
dep ensure
go build -o proxy
cp config_default.toml config.toml
```

After compiling the program, edit the `config.toml` file to your heart's content. For configuration, see [Configuration](#configuration)

### Requirements

* A computer to run this on
* Godep
* Go version 1.10 or greater
* Git

## Structure

The project is organized into several parts. The main initialization and listen steps (`server.go`), type definitions (`typedef`), the proxy itself (`proxy`), the message queue manager (`mq`), the compression steps (`compression`), and the various MQ services the application supports.

## Message Queue Services

Currently, the application supports the following MQ services:

* Google PubSub (`google`)

If you do not see a MQ service listed, please feel free to create a feature request in Issues, or, if you're particularly ambitious, try and implement it yourself!

## Configuration

All configuration is contained in the `config.toml` file.

### Proxy server

Here is a sample proxy server configuration, along with a list of all possible configuration options

```toml
port = 8080
host = "0.0.0.0"
proxy_origin = "127.0.0.1:8081"
remote_ip_header = "X-Forwarded-For"
remote_ip_should_fallback = false
server_name = "proxy-server"
mq_service = "google"
log_level = "info"
tracked_cookies = ["cookie_1", "cookie_2"]
```

##### port - _Required if listen_unix not provided_

This is the port the server will listen on

##### host -  _Required if listen_unix not provided_

The bind address for the server (usually, just leave it as `0.0.0.0`)

##### listen_unix  - _Required if host not provided_

UNIX socket to listen on (ex. `proxy.sock`)

##### proxy_origin - _Required  if proxy_origin_unix not provided_

The server to pass the requests to. Cannot be used for UNIX origins

##### proxy_origin_unix - _Required if proxy_origin not provided_

UNIX address of the server to pass the requests to. Cannot be used for TCP origins

##### remote_ip_header  - _Optional_

The header that contains the remote client IP. If clients connect directly to this server, then leave it blank or missing

##### remote_ip_should_fallback - _Optional_

If there does not exist a client IP header (and one was specified), then use the connecting IP as the remote IP field in the request metadata

##### server_name - _Required_

Server (proxy) name to include in the metadata. If left empty, then no server name will be passed

##### mq_service - _Required_

Name of the MQ service the proxy will use

##### log_level - _Optional_

Level of messages to log. Case in-sensitive. One of:

* trace
* debug
* info (default)
* warn
* error
* fatal
* panic

##### log_format - _Optional_

Format of messages to log. Case-insensitive. One of:

* text
* json

##### tracked_cookies - _Optional_

Any cookies whose content you would like to have tracked

### MQ Service

#### Amazon SQS

*Note: It is possible Amazon SQS will require additional environment variables to function properly*

```toml
[ MQServiceConfig ]
queue_url = "	https://sqs.us-west-2.amazonaws.com/1234567890/queue-name"
```

##### queue_url

URL of the queue to use - can be retrieved from the SQS console


#### Google PubSub

*Note: It is possible Google PubSub will require additional environment variables to function properly*

```toml
[ MQServiceConfig ]
pubsub_topic_name = "topic_name"
pubsub_project_name = "project_name"
```

##### pubsub_topic_name

Name of the PubSub topic to publish to

##### pubsub_project_name

Name of the GCP project to use

## Contributing a new MQ service

If you would like to contribute your own MQ service to the project, it's relatively simple and easy. There are three main parts

### Code

All MQ services must implement the following methods:

```golang
func PushData(content []byte);
```

From there, you can do effectively anything you want. The message 

### Config

Any and all config values can be stored in a TOML sub section. For example, a GCP config:

```toml
[ MQServiceConfig ]
pubsub_topic_name = "topic_name"
pubsub_project_name = "project_name"
```

The configuration will be passed as a `map[string]string`, containing any values stored in a `MQServiceConfig` section. It is recommended to use this to configure the MQ service.

### Selection

When the proxy is initialized, the configuration variable `mq_service` from the config file is passed, to allow the `mq/messagehandler.go` file to determine what handler to use.

There is a switch statement in the above mentioned file, containing all supported options. Each case contains an init step (to initialize the message queue), and a record step (saving the function to be used later). The name of the function to push data, dependent on the MQ service type, is stored in the `messageHandler` variable. Here is its declaration:

```golang
var messageHandler func(data []byte)
```

Simply, to add your own case statement, it needs three lines:

```golang
case "mq-service-name":
    messageHandler = mqservice.MQServicePushDataFunction
    mq_service.Init(mqServiceConfig)
```

The `messageHandler` variable is mentioned above. Do not add parenthesis after naming your function, as the variable wants to store a reference to the function, not the result.

The `mqServiceConfig` variable is of type `map[string]string`, and contains all the toml config data. It is up to you to parse this data as you please.

Those three lines can be tacked on just before the `default` statement, in `mq/messagehandler.go`, in the function `Init`

## Bugs

If you find a bug, please feel free to report it in the issues section!

### Security vulnerabilities

While this code is relatively simple, and should not be subject to many vulternabilities, it is always possible to have some. If you find one, you are welcome to do one of two things:

1. Open an issue on GitLab (for non-serious vulnerabilities)
2. Email us directly (for serious vulnerabilities)

Please do not email us about support, feature requests, or any other non-critical issues. If you would like enterprise support for this product, please contact us at `company@demilletech.net`

Email for critical vulnerabilities: `security@demilletech.net`

## Contributing

Feel free to contribute code as you please. However, before you do:

1. Please ensure all added functionality is tested and works properly
2. Do not submit incomplete code
3. Ensure it is formatted according to Go's styling guidelines

## API Stability

At the moment, the _guaranteed_ stability is that the `request-id`/`Request-ID` headers will always contain a string value, representing a unique ID assigned to the request. It will also transparently pass all headers, body content, and status codes.

In terms of internal stability, there are no guarantees made, as I may decide to change stuff on a whim.

## Limitations

At the moment, this library does _not_ support WebSockets