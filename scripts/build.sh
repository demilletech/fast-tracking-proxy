
set -e -u -x
set -o pipefail

GIT_VERSION=$(cat .git/ref)

mkdir bin

dep ensure
go build -o bin/proxy-server

mkdir assets

tar -cvf assets/fast-tracking-proxy.$GIT_VERSION.linux-amd64.tar.gz bin/proxy-server
