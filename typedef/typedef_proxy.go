package typedef

import "time"

type Request struct {
	RequestID      string            `json:"request-id"`
	RemoteIP       string            `json:"remote-ip"`
	ConnectingIP   string            `json:"connecting-ip"`
	Host           string            `json:"host"`
	Path           string            `json:"path"`
	ResponseCode   int               `json:"response-code"`
	QueryParams    string            `json:"query-params"`
	Method         string            `json:"method"`
	DNT            int               `json:"dnt"`
	Referer        string            `json:"referer"`
	UserAgent      string            `json:"user-agent"`
	TrackedCookies map[string]string `json:"tracked-cookies"`
	Date           time.Time         `json:"date"`
}
