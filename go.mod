module gitlab.com/demilletech/fast-tracking-proxy

require (
	cloud.google.com/go v0.37.2
	github.com/BurntSushi/toml v0.3.1
	github.com/aws/aws-sdk-go v1.19.43
	github.com/golang/protobuf v1.3.1
	github.com/googleapis/gax-go v2.0.0+incompatible
	github.com/hashicorp/golang-lru v0.5.1
	github.com/klauspost/compress v1.4.1
	github.com/klauspost/cpuid v1.2.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.2.0
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fasthttp v1.2.0
	github.com/valyala/gozstd v1.3.0
	github.com/valyala/tcplisten v0.0.0-20161114210144-ceec8f93295a
	go.opencensus.io v0.19.2
	golang.org/x/exp v0.0.0-20190221220918-438050ddec5e // indirect
	golang.org/x/net v0.0.0-20190328230028-74de082e2cca
	golang.org/x/oauth2 v0.0.0-20190319182350-c85d3e98c914
	golang.org/x/sync v0.0.0-20190227155943-e225da77a7e6
	golang.org/x/sys v0.0.0-20190329044733-9eb1bfa1ce65
	golang.org/x/text v0.3.1-0.20180807135948-17ff2d5776d2
	google.golang.org/api v0.3.0
	google.golang.org/appengine v1.5.0
	google.golang.org/genproto v0.0.0-20190401181712-f467c93bbac2
	google.golang.org/grpc v1.19.1
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
