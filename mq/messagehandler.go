package mq

import (
	"encoding/json"
	"sync"

	"github.com/sirupsen/logrus"
	"gitlab.com/demilletech/fast-tracking-proxy/amazon"
	"gitlab.com/demilletech/fast-tracking-proxy/compression"
	"gitlab.com/demilletech/fast-tracking-proxy/conf"
	"gitlab.com/demilletech/fast-tracking-proxy/google"
	"gitlab.com/demilletech/fast-tracking-proxy/typedef"
)

const messagesArrayLength = 1024

var addmux *sync.Mutex
var messages []string
var messagecounter = 0
var run = true

var messageHandler func(data []byte)

func Init() {
	logrus.Debugf("Initializing MQ service: %s\n", conf.ServerConfiguration.MQService)
	messages = make([]string, messagesArrayLength)
	addmux = &sync.Mutex{}

	switch conf.ServerConfiguration.MQService {
	case "google":
		messageHandler = google.PushData
		google.Init(conf.ServerConfiguration.MQServiceConfig)
	case "amazon":
		messageHandler = amazon.PushData
		amazon.Init(conf.ServerConfiguration.MQServiceConfig)
	default:
		logrus.Panic("Incomprehensible MQService parameter")
	}
	logrus.Debug("Completed initializing MQ service")
}

func AddRequest(request *typedef.Request) {
	bytes, err := json.Marshal(request)
	if err != nil {
		logrus.Error(err)
		return
	}

	AddMessage(string(bytes))
	checkMQ()
}

func AddMessage(body string) {
	logrus.Trace("Adding message to queue")
	addmux.Lock()
	defer addmux.Unlock()

	messages[messagecounter] = body
	messagecounter++
	logrus.Trace("Done adding message to queue")
}

func pushMessages() {
	logrus.Trace("Pushing messages from queue")
	addmux.Lock()
	if messagecounter == 0 {
		addmux.Unlock()
		return
	}

	clone := messages
	messages = make([]string, messagesArrayLength)
	messagecounter = 0

	logrus.Trace("Unlocking addmux")
	addmux.Unlock()

	outstr := "["
	for i, e := range clone {
		if e != "" {
			if i > 0 {
				outstr += ","
			}
			outstr += e // Must be proper json
		}
	}
	outstr += "]"

	compressed := compression.CompressData([]byte(outstr))

	messageHandler(compressed)
}
func Close() {
	logrus.Info("Flushing & closing MQ")
	run = false
	pushMessages()
}

func checkMQ() {
	if messagecounter > messagesArrayLength/2 {
		go pushMessages()
	}
}
