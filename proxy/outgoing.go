package proxy

import (
	"net"
	"strings"

	"gitlab.com/demilletech/fast-tracking-proxy/conf"

	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

func MakeRequest(origin, reqid string, srcreq *fasthttp.Request, resp *fasthttp.Response) {
	req := fasthttp.AcquireRequest()
	srcreq.CopyTo(req)
	req.SetHost(origin)

	req.Header.Set("request-id", reqid)

	c := fasthttp.Client{}

	if conf.ServerConfiguration.IsUnixOrigin {
		logrus.WithField("reqid", reqid).Trace("Creating UNIX client")
		c.Dial = func(addr string) (net.Conn, error) {
			logrus.Debugf("Dialing unix %s\n", origin)
			return net.Dial("unix", origin)
		}
	}

	logrus.Trace(reqid, "Performing request")
	err := c.Do(req, resp)
	if err != nil {
		errtext := err.Error()
		logrus.WithField("reqid", reqid).Debugf("Request failed: %s\n", errtext)
		if strings.HasSuffix(errtext, "connect: connection refused") {
			logrus.WithField("reqid", reqid).Debug("Got `connection refused`")
			resp.SetStatusCode(503)
		} else if strings.HasSuffix(errtext, "no such host") {
			logrus.WithField("reqid", reqid).Debug("Got `no such host` on request")
			resp.SetStatusCode(503)
		} else if err != nil {
			logrus.Error(err)
		}
	}
}
