package proxy

import (
	"strconv"
	"time"

	"gitlab.com/demilletech/fast-tracking-proxy/conf"

	"github.com/sirupsen/logrus"

	uuid "github.com/satori/go.uuid"
	"github.com/valyala/fasthttp"
	"gitlab.com/demilletech/fast-tracking-proxy/mq"
	"gitlab.com/demilletech/fast-tracking-proxy/typedef"
)

func HandleRequest(ctx *fasthttp.RequestCtx) {
	reqid := GenUUID()
	logrus.WithField("reqid", reqid).Debug("About to handle request")
	request := &typedef.Request{
		RequestID:      reqid,
		ConnectingIP:   ctx.RemoteAddr().String(),
		Host:           string(ctx.Host()),
		Path:           string(ctx.Path()),
		QueryParams:    ctx.QueryArgs().String(),
		Method:         string(ctx.Method()),
		UserAgent:      string(ctx.UserAgent()),
		TrackedCookies: make(map[string]string),
		Date:           time.Now().UTC(),
		DNT:            0,
	}

	dnt := GetHeaderSafe("dnt", ctx)
	dntval, err := strconv.Atoi(string(dnt))
	if err == nil {
		request.DNT = dntval
	}

	request.Referer = GetHeaderSafe("referer", ctx)

	if conf.ServerConfiguration.RemoteIPHeader != "" {
		remoteIP := GetHeaderSafe(conf.ServerConfiguration.RemoteIPHeader, ctx)
		if remoteIP != "" {
			request.RemoteIP = remoteIP
		} else if conf.ServerConfiguration.RemoteIPShouldFallback {
			request.RemoteIP = request.ConnectingIP
		}
	} else {
		request.RemoteIP = request.ConnectingIP
	}

	logrus.WithField("reqid", reqid).Trace("Saving cookies for")
	for _, cookieName := range conf.ServerConfiguration.TrackedCookies {
		cookie := ctx.Request.Header.Cookie(cookieName)
		if cookie == nil {
			continue
		}
		request.TrackedCookies[cookieName] = string(cookie)
	}

	logrus.WithField("reqid", reqid).Trace("Making origin request")
	resp := fasthttp.AcquireResponse()
	MakeRequest(conf.ServerConfiguration.ProxyOrigin, request.RequestID, &ctx.Request, resp)

	request.ResponseCode = resp.StatusCode()
	resp.Header.Set("Request-ID", request.RequestID)

	logrus.WithField("reqid", reqid).Trace("Writing response body")
	ctx.Write(resp.Body())

	resp.CopyTo(&ctx.Response)

	go mq.AddRequest(request)

	logrus.WithField("reqid", reqid).Debug("Completed handling request")
}

func GetHeaderSafe(key string, ctx *fasthttp.RequestCtx) string {
	logrus.WithField("header", key).Trace("Grabbing header")
	rawval := ctx.Request.Header.Peek(key)
	logrus.WithField("header", key).Tracef("Got header")
	if rawval != nil {
		val := string(rawval)
		return val
	}
	return ""
}

func GenUUID() string {
	logrus.Trace("Generating UUID")
	uuid := uuid.NewV4()
	logrus.WithField("uuid", uuid).Trace("Generating UUID completed")
	return uuid.String()
}
