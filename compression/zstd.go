package compression

import (
	"github.com/sirupsen/logrus"
	zstd "github.com/valyala/gozstd"
)

func CompressData(data []byte) []byte {
	logrus.Trace("Compressing data")
	return zstd.Compress(nil, data)
}
