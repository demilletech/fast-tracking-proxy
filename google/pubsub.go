package google

import (
	"context"

	"cloud.google.com/go/pubsub"
	"github.com/sirupsen/logrus"
)

var PubsubTopicName string
var PubsubProjectName string
var pubsubClient *pubsub.Client
var pubsubTopic *pubsub.Topic
var pubsubSubscription *pubsub.Subscription

func Init(mqServiceConfig map[string]string) {
	logrus.Infof("Initializing Google PubSub")
	var ok bool
	if PubsubTopicName, ok = mqServiceConfig["pubsub_topic_name"]; !ok {
		logrus.Panic("pubsub_topic_name missing from config")
	}
	if PubsubProjectName, ok = mqServiceConfig["pubsub_project_name"]; !ok {
		logrus.Panic("pubsub_project_name missing from config")
	}
	ctx := context.Background()
	pubsubClient, err := pubsub.NewClient(ctx, PubsubProjectName)
	if err != nil {
		logrus.Fatal(err)
	}

	pubsubTopic = pubsubClient.Topic(PubsubTopicName)

}

func googlePushData(data []byte) {
	logrus.Trace("Pushing data to PubSub")
	ctx := context.Background()
	result := pubsubTopic.Publish(ctx, &pubsub.Message{
		Data: data,
	})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	logrus.Trace("Waiting for data to complete push")
	id, err := result.Get(ctx)
	if err != nil {
		logrus.Error(err)
	}
	logrus.Infof("Published a message; msg ID: %v\n", id)
}

func PushData(data []byte) {
	googlePushData(data)
}
